document.querySelector("form").addEventListener("submit", (e) => {
    e.preventDefault();

    let success = formValidator(e);

    if(success) {
        e.currentTarget.submit();
    }
});

function formValidator(e) {
    let form = e.currentTarget;
    let success = true;

    //Validate name input
    let nameInput = form.querySelector("input[name=\"name\"]");
    if(nameInput.value.length === 0) {
        alert("Nevyplněná hodnota Jméno");
        success = false;
    }

    //Validate password input
    let passwordInput = form.querySelector("input[name=\"password\"]");
    if(passwordInput.value.length < 8) {
        alert("Příliš krátké heslo");
        success = false;
    }

    //Validate native number input
    let nnInput = form.querySelector("input[name=\"nn\"]");
    let regex = new RegExp("[0-9]{6}/[0-9]{3,4}");
    if(regex.test(nnInput.value) === false) {
        alert("Neplatné rodné číslo");
        success = false;
    }

    //Validate password input
    let ageInput = form.querySelector("input[name=\"age\"]");
    if(ageInput.value < 18) {
        alert("Registrace je povolena od 18 let");
        success = false;
    }

    return success;
}
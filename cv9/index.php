<?php 
    include("./heslo.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Deváté cvičení</title>
    <style>
        label, button {
            display: block;
        }

        button {
            margin-top: 10px;
        }

        .form-warning {
            color: red;
        }

        .form-warning::before {
            content: "! ";
        }

        table {
            border: 1px solid black;
            margin-top: 30px;
        }
    </style>
</head>
<body>
    <p><a href="../">Rozcesník</a></p>
    <h1>Formulář:</h1>
    <form method="POST">
        <!-- Not empty -->
        <label for="name">Zadejte jméno:</label>
        <input type="text" name="name" value="<?php if(isset($_POST["name"])) echo(htmlspecialchars($_POST["name"]));?>"/>

        <!-- Length -->
        <label for="name">Místo bydliště:</label>
        <input type="text" name="city" value="<?php if(isset($_POST["city"])) echo(htmlspecialchars($_POST["city"]));?>" />

        <!-- Regex -->
        <label for="name">Zadejte rodné číslo:</label>
        <input type="text" name="pin" value="<?php if(isset($_POST["pin"])) echo(htmlspecialchars($_POST["pin"]));?>"/>

        <!-- Range -->
        <label for="name">Zadejte den v měsíci:</label>
        <input type="number" name="day" value="<?php if(isset($_POST["day"])) echo(htmlspecialchars($_POST["day"]));?>"/>

        <button>Odeslat</button>
    </form>

    <?php 
        if($_POST && !(isset($_POST["password_length"]))) {
            $error = false;
            //Name
            if(!(isset($_POST["name"]) && strlen($_POST["name"]) > 0)) {
                $error = true;
                print('<p class="form-warning">Prázdná hodnota u jména</p>');
            }

            //Nejkratší název obce jsou 2 znaky

            if(!(isset($_POST["city"]) && strlen($_POST["city"]) >= 2)) {
                $error = true;
                print('<p class="form-warning">Příliš krátký název obce</p>');
            }

            if(!(isset($_POST["pin"]) && preg_match("#[0-9]{6}[/][0-9]{3,4}#",$_POST["pin"]) == 1)) {
                $error = true;
                print('<p class="form-warning">Neplatný formát rodného čísla</p>');
            }

            if(!(isset($_POST["day"]) && is_numeric($_POST["day"]) && $_POST["day"] <= 31 && $_POST["day"] > 0)) {
                $error = true;
                print('<p class="form-warning">Neplatná hodnota pro den v měsíci</p>');
            }

            if($error === false) {
                echo('
                    <table>
                        <tr>
                            <td>Jméno:</td>
                            <td>' . htmlspecialchars($_POST["name"]) . '</td>
                        </tr>

                        <tr>
                            <td>Bydliště:</td>
                            <td>' . htmlspecialchars($_POST["city"]) . '</td>
                        </tr>

                        <tr>
                            <td>Rodné číslo:</td>
                            <td>' . htmlspecialchars($_POST["pin"]) . '</td>
                        </tr>

                        <tr>
                        <td>Den v měsíci:</td>
                        <td>' . htmlspecialchars($_POST["day"]) . '</td>
                    </tr>
                    </table>
                ');
            }
        }
    ?>

    <hr />
    <h1>Heslo</h1>
    <form method="POST">
        <label>Zadejte délku hesla:</label>
        <input type="number" name="password_length"/>
        <button type="submit">Generovat</button>
    </form>
    <?php 
        if(isset($_POST["password_length"]) && is_numeric($_POST["password_length"]) && $_POST["password_length"] > 0) {
            echo("<p>" . passwordGenerator($_POST["password_length"]) . "</p>");
        }
    ?>
</body>
</html>
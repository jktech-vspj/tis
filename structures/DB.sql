-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Počítač: localhost
-- Vytvořeno: Sob 10. pro 2022, 21:49
-- Verze serveru: 10.9.4-MariaDB
-- Verze PHP: 8.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `hrdlic30`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `filmy`
--

CREATE TABLE `filmy` (
  `id_filmy` int(11) NOT NULL,
  `nazev` varchar(255) NOT NULL,
  `rezie` varchar(255) NOT NULL,
  `scenar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Vypisuji data pro tabulku `filmy`
--

INSERT INTO `filmy` (`id_filmy`, `nazev`, `rezie`, `scenar`) VALUES
(1, 'Casino Royale', 'Martin Campbell', 'Neal Purvis'),
(4, 'Quantum of Solace', 'Marc Forster', 'Neal Purvis'),
(5, 'Skyfall', 'Sam Mendes', 'John Logan'),
(6, 'Spectre', 'Sam Mendes', 'John Logan'),
(7, 'Není čas zemřít', 'Není čas zemřít', 'Neal Purvis');

-- --------------------------------------------------------

--
-- Struktura tabulky `projekt_drzitele`
--

CREATE TABLE `projekt_drzitele` (
  `id_drzitele` int(11) NOT NULL,
  `jmeno` varchar(255) NOT NULL,
  `prijmeni` varchar(255) NOT NULL,
  `cislo_op` int(11) NOT NULL,
  `skupina` int(11) NOT NULL,
  `datum_narozeni` date NOT NULL,
  `datum_platnosti` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Vypisuji data pro tabulku `projekt_drzitele`
--

INSERT INTO `projekt_drzitele` (`id_drzitele`, `jmeno`, `prijmeni`, `cislo_op`, `skupina`, `datum_narozeni`, `datum_platnosti`) VALUES
(30, 'Filip', 'Karban', 921347856, 4, '1987-12-22', '2026-07-10');

-- --------------------------------------------------------

--
-- Struktura tabulky `projekt_skupiny`
--

CREATE TABLE `projekt_skupiny` (
  `id_skupiny` int(11) NOT NULL,
  `nazev` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Vypisuji data pro tabulku `projekt_skupiny`
--

INSERT INTO `projekt_skupiny` (`id_skupiny`, `nazev`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'C'),
(4, 'D'),
(5, 'E');

-- --------------------------------------------------------

--
-- Struktura tabulky `projekt_uzivatele`
--

CREATE TABLE `projekt_uzivatele` (
  `id_uzivatele` int(11) NOT NULL,
  `nazev` varchar(255) NOT NULL,
  `heslo` varchar(255) NOT NULL,
  `admin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Vypisuji data pro tabulku `projekt_uzivatele`
--

INSERT INTO `projekt_uzivatele` (`id_uzivatele`, `nazev`, `heslo`, `admin`) VALUES
(1, 'admin', '$2y$10$rCVAZrpjHGaQTjVjsIedBu5ce.Sg894wQPsSvhRS5mA0L18S4k2zC', 1),
(2, 'user', '$2y$10$oD3JRipO2wx58Oic4nez0OaRJ5/6gJvpiG0dy/87rcLG6JETe4MNC', NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `projekt_zbrane`
--

CREATE TABLE `projekt_zbrane` (
  `id_zbrane` int(11) NOT NULL,
  `znacka` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `raze` varchar(10) NOT NULL,
  `vyrobni_cislo` varchar(8) NOT NULL,
  `drzitel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Vypisuji data pro tabulku `projekt_zbrane`
--

INSERT INTO `projekt_zbrane` (`id_zbrane`, `znacka`, `model`, `raze`, `vyrobni_cislo`, `drzitel`) VALUES
(11, 'FN', 'Five-seveN', '5.7×28mm', 'EA52751', 30);

-- --------------------------------------------------------

--
-- Struktura tabulky `tabulka`
--

CREATE TABLE `tabulka` (
  `cislo` int(11) NOT NULL DEFAULT 0,
  `jmeno` varchar(30) CHARACTER SET latin2 COLLATE latin2_czech_cs NOT NULL DEFAULT '',
  `prijmeni` varchar(30) CHARACTER SET latin2 COLLATE latin2_czech_cs NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Vypisuji data pro tabulku `tabulka`
--

INSERT INTO `tabulka` (`cislo`, `jmeno`, `prijmeni`) VALUES
(42, 'dsilfkjklsdf', 'asdads'),
(50, 'asdasd', 'asdasd');

-- --------------------------------------------------------

--
-- Struktura tabulky `zp_drzitele`
--

CREATE TABLE `zp_drzitele` (
  `id_drzitele` int(11) NOT NULL,
  `jmeno` varchar(255) NOT NULL,
  `prijmeni` varchar(255) NOT NULL,
  `cislo_op` int(11) NOT NULL,
  `skupina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Vypisuji data pro tabulku `zp_drzitele`
--

INSERT INTO `zp_drzitele` (`id_drzitele`, `jmeno`, `prijmeni`, `cislo_op`, `skupina`) VALUES
(1, 'Petr', 'Volný', 10569, 1),
(2, 'Aleš', 'Moravec', 26843, 3),
(3, 'Adam', 'Zouhar', 58412, 5),
(4, 'Pavel', 'Nový', 85431, 3),
(5, 'Milan', 'Volný', 58314, 1),
(7, 'Petra', 'Janáčková', 87425, 1),
(8, 'Alex', 'Navrátil', 58743, 4),
(9, 'Jan', 'Karták', 871245, 5);

-- --------------------------------------------------------

--
-- Struktura tabulky `zp_skupiny`
--

CREATE TABLE `zp_skupiny` (
  `id_skupiny` int(11) NOT NULL,
  `nazev` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Vypisuji data pro tabulku `zp_skupiny`
--

INSERT INTO `zp_skupiny` (`id_skupiny`, `nazev`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'C'),
(4, 'D'),
(5, 'E');

-- --------------------------------------------------------

--
-- Struktura tabulky `zp_uzivatele`
--

CREATE TABLE `zp_uzivatele` (
  `id_uzivatele` int(11) NOT NULL,
  `jmeno` varchar(255) NOT NULL,
  `heslo` varchar(255) NOT NULL,
  `admin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Vypisuji data pro tabulku `zp_uzivatele`
--

INSERT INTO `zp_uzivatele` (`id_uzivatele`, `jmeno`, `heslo`, `admin`) VALUES
(1, 'test', '$2y$10$cxUcaCRkL3piBc4PFR0dnOKsvx5StNk.9dpgMKoJgLHrNEDXXxgvq', NULL),
(2, 'admin', '$2y$10$eHnUPT1lAbz6re37zp6uAedfUUUPUxB27pzlhn84rRTf.dUomKqJy', 1);

--
-- Indexy pro exportované tabulky
--

--
-- Indexy pro tabulku `filmy`
--
ALTER TABLE `filmy`
  ADD PRIMARY KEY (`id_filmy`);

--
-- Indexy pro tabulku `projekt_drzitele`
--
ALTER TABLE `projekt_drzitele`
  ADD PRIMARY KEY (`id_drzitele`);

--
-- Indexy pro tabulku `projekt_skupiny`
--
ALTER TABLE `projekt_skupiny`
  ADD PRIMARY KEY (`id_skupiny`);

--
-- Indexy pro tabulku `projekt_uzivatele`
--
ALTER TABLE `projekt_uzivatele`
  ADD PRIMARY KEY (`id_uzivatele`);

--
-- Indexy pro tabulku `projekt_zbrane`
--
ALTER TABLE `projekt_zbrane`
  ADD PRIMARY KEY (`id_zbrane`);

--
-- Indexy pro tabulku `tabulka`
--
ALTER TABLE `tabulka`
  ADD PRIMARY KEY (`cislo`);

--
-- Indexy pro tabulku `zp_drzitele`
--
ALTER TABLE `zp_drzitele`
  ADD PRIMARY KEY (`id_drzitele`);

--
-- Indexy pro tabulku `zp_skupiny`
--
ALTER TABLE `zp_skupiny`
  ADD PRIMARY KEY (`id_skupiny`);

--
-- Indexy pro tabulku `zp_uzivatele`
--
ALTER TABLE `zp_uzivatele`
  ADD PRIMARY KEY (`id_uzivatele`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `filmy`
--
ALTER TABLE `filmy`
  MODIFY `id_filmy` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pro tabulku `projekt_drzitele`
--
ALTER TABLE `projekt_drzitele`
  MODIFY `id_drzitele` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pro tabulku `projekt_skupiny`
--
ALTER TABLE `projekt_skupiny`
  MODIFY `id_skupiny` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pro tabulku `projekt_uzivatele`
--
ALTER TABLE `projekt_uzivatele`
  MODIFY `id_uzivatele` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `projekt_zbrane`
--
ALTER TABLE `projekt_zbrane`
  MODIFY `id_zbrane` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pro tabulku `zp_drzitele`
--
ALTER TABLE `zp_drzitele`
  MODIFY `id_drzitele` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pro tabulku `zp_uzivatele`
--
ALTER TABLE `zp_uzivatele`
  MODIFY `id_uzivatele` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

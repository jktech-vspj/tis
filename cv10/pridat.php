<?php
    require("connect.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Přidání nového filmu</title>
    <style>
        label {
            display: block;
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <h1>Přidání nového filmu</h1>
    <form action="insert.php" method="POST">
        <label>
            Název:
            <input type=text name="movie_name">
        </label>

        <label>
            Režie:
            <input type=text name="movie_director">
        </label>

        <label>
            Scénář:
            <input type=text name="movie_scriptwriter">
        </label>

        <button type="submit">Přidat</button>
    </form>
    <p><a href="./">Zpět</a></p>
</body>
</html>

<?php 
    mysqli_close($spojeni);
?>

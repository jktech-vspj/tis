<?php
  require("connect.php");

  $sql = "select * from filmy";
  $vysledek = mysqli_query($spojeni, $sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Databáze fiilmů</title>
  <style>
    tr th {
      background-color: aqua;
    }

    tr:nth-child(even) td {
      background-color: #dddddd;
    }

    tr:nth-child(odd) td {
      background-color: #ffffff;
    }
  </style>
</head>
<body>
  <h1>Databáze filmů</h1>
  <a href="pridat.php">Přidání nového filmu</a>
  <table border="0">
    <tr>
      <th>#</th>
      <th>Název</th>
      <th>Režie</th>
      <th>Scénář</th>
    </tr>
    <?php 
    $i = 1;
      if (mysqli_num_rows($vysledek) > 0) {
        while ($radek = mysqli_fetch_assoc($vysledek)) {
          echo('
            <tr>
              <td>'. $i . '</td>
              <td>'. $radek["nazev"] . '</td>
              <td>'. $radek["rezie"] . '</td>
              <td>'. $radek["scenar"] . '</td>
              <td><a href="smazat.php?id=' . $radek["id_filmy"] . '">Smazat</a></td>
              <td><a href="upravit.php?id=' . $radek["id_filmy"] . '">Upravit</a></td>
            </tr>
          ');

          $i++;
        }
      }
    ?>
  </table>
</body>
</html>

<?php 
  mysqli_close($spojeni);
?>
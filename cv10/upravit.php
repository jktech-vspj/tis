<?php
  require("connect.php");
  $sql = "select * from filmy WHERE id_filmy=" . htmlspecialchars($_GET["id"]);
  $vysledek = mysqli_query($spojeni, $sql);
  $radek = mysqli_fetch_assoc($vysledek);

  mysqli_close($spojeni);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Upravení filmu</title>
    <style>
        label {
            display: block;
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <h1>Upravení filmu</h1>
    <form action="update.php" method="POST">
        <input type="hidden" name="movie_id" value="<?php echo $radek["id_filmy"] ?>">

        <label>
            Název:
            <input type=text name="movie_name" value="<?php echo $radek["nazev"] ?>">
        </label>

        <label>
            Režie:
            <input type=text name="movie_director" value="<?php echo $radek["rezie"] ?>">
        </label>

        <label>
            Scénář:
            <input type=text name="movie_scriptwriter" value="<?php echo $radek["scenar"] ?>">
        </label>

        <button type="submit">Upravit</button>
    </form>
    <p><a href="./">Zpět</a></p>
</body>
</html>

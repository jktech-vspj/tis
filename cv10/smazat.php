<?php
  require("connect.php");
  $sql = "select * from filmy WHERE id_filmy=" . htmlspecialchars($_GET["id"]);
  $vysledek = mysqli_query($spojeni, $sql);
  $radek = mysqli_fetch_assoc($vysledek);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Potvrzení smazání filmu</title>
    <style>
        label {
            display: block;
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <h1>Potvrzení smazání filmu</h1>
    <p>Opravdu chcete smazat film <b><?php echo($radek['nazev'])?> </b>?</p>
    <form action="delete.php" method="POST">
        <input type="hidden" name="movie_id" value="<?php echo $radek["id_filmy"] ?>"/>
        <button type="submit">Smazat</button>
    </form>
    <p><a href="./">Zpět</a></p>
</body>
</html>

<?php 
    mysqli_close($spojeni);
?>
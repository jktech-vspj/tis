<?php 
    require("connect.php");

    $sql = "
        SELECT *, zp_skupiny.nazev as skupina
        FROM zp_drzitele
        INNER JOIN zp_skupiny on zp_drzitele.skupina = zp_skupiny.id_skupiny
    ";

    $drzitele = mysqli_query($spojeni, $sql);

    $drziteleFinal = [];

    while($radek = mysqli_fetch_assoc($drzitele)) {
        array_push($drziteleFinal, $radek);
    }

    echo(json_encode($drziteleFinal));
?>
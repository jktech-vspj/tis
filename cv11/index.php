<?php 
    require("connect.php");

    if(isset($_GET["q"]) && $_GET["q"]) {
        $where = "jmeno LIKE '" . htmlspecialchars($_GET["q"]) . "%' OR prijmeni LIKE '" . htmlspecialchars($_GET["q"]) . "%'";
    }

    else {
        $where = "id_drzitele > 0";
    }

    if(isset($_GET["order"]) && $_GET["order"]) {
        $order = "zp_drzitele." . $_GET["order"];
    }

    else {
        $order = "id_drzitele";
    }

    if(isset($_GET["direction"]) && $_GET["direction"] && (strtolower($_GET["direction"]) === "asc" || strtolower($_GET["direction"]) === "desc")) {
        $direction = $_GET["direction"];
    }

    else {
        $direction = "ASC";
    }

    $sql = "
        SELECT *, zp_skupiny.nazev as skupina
        FROM zp_drzitele
        INNER JOIN zp_skupiny on zp_drzitele.skupina = zp_skupiny.id_skupiny
        WHERE " . $where . " 
        ORDER BY " . $order . " " . $direction ."
    ";

    $drzitele = mysqli_query($spojeni, $sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Databáze držitelů zbrojního průkazu</title>
    <link href="./public/style/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <h1>Databáze držitelů zbrojního průkazu</h1>

    <div id="data-controls">
        <span>
            <?php 
                if($_SESSION["uzivatel_admin"] === true) {
                    echo('<a href="addUser.php" class="add-user">Přidat držitele</a>');
                }
            ?>
            <a href="display-json.php" class="to-json">Zobrazit data z tabulek</a>
            <a href="display-pdf.php" class="to-pdf">Exportovat do PDF</a>
        </span>
        <label>
            <input type="text" id="search-name-input" placeholder="Vyhledat podle jména" value="<?php if(isset($_GET["q"])) echo(htmlspecialchars($_GET["q"])); ?>"/>
            <button class="search" id="search-button"></button>
            <a href="./login.php?logout" class="logout" title="Odhlásit se"></a>
        </label>
    </div>

    <table>
        <tr>
            <th data-order-name="id_drzitele">
                ID
                <button type="button" class="order-desc"></button>
                <button type="button" class="order-asc"></button>
            </th>
            <th data-order-name="jmeno">
                Jméno
                <button type="button" class="order-desc"></button>
                <button type="button" class="order-asc"></button>
            </th>
            <th data-order-name="prijmeni">
                Přijmení
                <button type="button" class="order-desc"></button>
                <button type="button" class="order-asc"></button>
            </th>
            <th data-order-name="cislo_op">
                Číslo OP
                <button type="button" class="order-desc"></button>
                <button type="button" class="order-asc"></button>
            </th>
            <th data-order-name="skupina">
                Skupina
                <button type="button" class="order-desc"></button>
                <button type="button" class="order-asc"></button>
            </th>
            <th></th>
        </tr>

        <?php 
            $i = 0;
            if(mysqli_num_rows($drzitele) > 0) {
                while($radek = mysqli_fetch_assoc($drzitele)) {
                    echo('
                        <tr>
                            <td>'. $radek["id_drzitele"] . '</td>
                            <td>'. $radek["jmeno"] . '</td>
                            <td>'. $radek["prijmeni"] . '</td>
                            <td>'. $radek["cislo_op"] . '</td>
                            <td>'. $radek["skupina"] . '</td>
                            
                        ');

                        if($_SESSION["uzivatel_admin"] === true) {
                            echo('
                                <td>
                                    <a href="edit.php?id=' . $radek["id_drzitele"] . '" class="edit"></a>
                                    <a href="delete.php?id=' . $radek["id_drzitele"] . '" class="delete"></a>
                                </td>
                            ');
                        }

                    echo(' 
                        </tr>
                    ');

                    $i++;
                }
            }

            else {
                echo('
                    <tr class="empty">
                        <td colspan="6">Držitel nebyl nalezen</td>
                    </tr>
                ');
            }
        ?>
    </table>
    <script src="./public/js/js.js"></script>
</body>
</html>
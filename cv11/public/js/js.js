//Search from name
document.querySelector("#search-button")?.addEventListener("click", () => {
    search();
});
document.querySelector("#search-name-input")?.addEventListener("keydown", (e) => {
    if(e.key === "Enter") {
        search();
    }
});

function search(order = "id_drzitele", direction = "ASC") {
    const query = document.querySelector("#search-name-input").value;
    let targetUrl = "./?q=" + encodeURIComponent(query);
    targetUrl += "&order=" + encodeURIComponent(order);
    targetUrl += "&direction=" + encodeURIComponent(direction);
    window.location.href = targetUrl;
}

document.querySelectorAll(".order-asc, .order-desc").forEach(element => {
    element.addEventListener("click", (e) => {
        if(e.target.classList.contains("order-asc")) {
            search(e.target.parentElement.dataset.orderName, "asc");
        }

        else {
            search(e.target.parentElement.dataset.orderName, "desc");
        }
    });
});

document.querySelectorAll(".delete").forEach(element => {
    element.addEventListener("click", (e) => {
        e.preventDefault();

        const acceptBox = document.createElement("span");
        acceptBox.classList.add("accept-remove");

        const acceptButton = document.createElement("button");
        acceptButton.innerText = "Smazat";
        acceptButton.onclick = () => {
            window.location.href = e.target.href;
        }
        acceptBox.appendChild(acceptButton);

        const rejectButton = document.createElement("button");
        rejectButton.innerText = "Zrušit";
        rejectButton.onclick = () => {
            acceptBox.remove();
        }
        acceptBox.appendChild(rejectButton);

        e.target.insertAdjacentElement("afterend", acceptBox);
    });
});

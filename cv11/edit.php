<?php 
    require("connect.php");

    if($_SESSION["uzivatel_admin"] !== true) {
        header("Location: ./");
        exit();
    }

    if($_POST) {
        if(
            isset($_POST["first_name"]) && $_POST["first_name"] &&
            isset($_POST["last_name"]) && $_POST["last_name"] &&
            isset($_POST["id_number"]) && $_POST["id_number"] && is_numeric($_POST["id_number"]) &&
            isset($_POST["group"]) && $_POST["group"] &&
            isset($_POST["id"]) && $_POST["id"] && is_numeric($_POST["id"])
        ) {
            $sql = "
                UPDATE zp_drzitele
                SET jmeno = '" . htmlspecialchars($_POST["first_name"]) . "', prijmeni = '" . htmlspecialchars($_POST["last_name"]) . "', cislo_op = '" . htmlspecialchars($_POST["id_number"]) . "', skupina = '" . htmlspecialchars($_POST["group"]) . "'
                WHERE id_drzitele = '" . htmlspecialchars($_POST["id"]) . "'
            ";

            if (mysqli_query($spojeni, $sql)) {
                header("Location: ./");
            }

            else {
                header("Location: edit.php?error=1&?id=" . $_POST["id"]);
            }

            exit();
        }

        else {
            header("Location: ./");
            exit();
        }
    }

    else if(isset($_GET["error"]) && $_GET["error"]) {
        switch ($_GET["error"]) {
            case 1: 
                $errorMessage = "Držitele se nepodařilo upravit";
                break;

            case 2: 
                $errorMessage = "Vyplňte všechny hodnoty";
                break;
        }
    }

    $sql = "
        SELECT *
        FROM zp_skupiny
    ";

    $skupiny = mysqli_query($spojeni, $sql);

    $sql = "
        SELECT zp_drzitele.*
        FROM zp_drzitele
        WHERE id_drzitele=" . $_GET["id"] . " 
    ";

    $drzitel = mysqli_query($spojeni, $sql);
    $drzitel = mysqli_fetch_assoc($drzitel);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Upravit držitele zbrojního průkazu</title>
    <link href="./public/style/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <div id="add-form">
        <form method="POST">
            <h1>Upravit držitele</h1>
            <input type="hidden" name="id" value="<?php echo($drzitel["id_drzitele"])?>" />
            <label>
                Zadejte jméno:
                <input type="text" name="first_name" value="<?php echo($drzitel["jmeno"])?>" required />
            </label>
            
            <label>
                Zadejte příjmení:
                <input type="text" name="last_name" value="<?php echo($drzitel["prijmeni"])?>" required />
            </label>
            
            <label>
                Zadejte číslo OP:
                <input type="number" name="id_number" min="10000" value="<?php echo($drzitel["cislo_op"])?>" required />
            </label>
    
            <label>
                Vyberte skupinu:
                <select name="group">
                    <?php 
                        while($radek = mysqli_fetch_assoc($skupiny)) {
                            if($radek["id_skupiny"] == $drzitel["skupina"]) {
                                echo('<option value="' . $radek["id_skupiny"] . '" selected>' . $radek["nazev"] . '</option>');
                            }

                            else {
                                echo('<option value="' . $radek["id_skupiny"] . '">' . $radek["nazev"] . '</option>');
                            }
                        }
                        ?>
                </select>
            </label>
            <?php 
                if(isset($errorMessage)) {
                    echo('<p class="error-message">' . $errorMessage . '</p>');
                }
            ?>
            <a href="./" class="back"> Zpět</a>
            <button type="submit">Upravit</button>
        </form>
    </div>
</body>
</html>
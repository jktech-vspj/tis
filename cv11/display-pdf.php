<?php
    require("connect.php");
    require("tfpdf/tfpdf.php");
    
    $sql = "
        SELECT *, zp_skupiny.nazev as skupina
        FROM zp_drzitele
        INNER JOIN zp_skupiny on zp_drzitele.skupina = zp_skupiny.id_skupiny
    ";

    $drzitele = mysqli_query($spojeni, $sql);

    $pdf = new tFPDF();
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->SetFont('DejaVu','',10);
    $pdf->AddPage();
    $pdf->Cell(10,10,"id",1);
    $pdf->Cell(50,10,"Jméno",1);    
    $pdf->Cell(50,10,"Příjmení",1);
    $pdf->Cell(30,10,"Skupina",1);
    $pdf->Cell(30,10,"Občanský průkaz",1);

    $pdf->Ln(); 

    while($radek = mysqli_fetch_assoc($drzitele)){
        $pdf->Cell(10, 10, $radek["id_drzitele"], 0);
        $pdf->Cell(50, 10,$radek["jmeno"] ,0);
        $pdf->Cell(50, 10, $radek["prijmeni"], 0);
        $pdf->Cell(30, 10, $radek["skupina"], 0);
        $pdf->Cell(30, 10,$radek["cislo_op"], 0);
        $pdf->Ln();
    }

    $pdf->Output();
    mysqli_close($connect);
?>

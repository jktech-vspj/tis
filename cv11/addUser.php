<?php 
    require("connect.php");

    if($_SESSION["uzivatel_admin"] !== true) {
        header("Location: ./");
        exit();
    }

    if($_POST) {
        if(
            isset($_POST["first_name"]) && $_POST["first_name"] &&
            isset($_POST["last_name"]) && $_POST["last_name"] &&
            isset($_POST["id_number"]) && $_POST["id_number"] && is_numeric($_POST["id_number"]) &&
            isset($_POST["group"]) && $_POST["group"]
        ) {
            $sql = "
                INSERT INTO zp_drzitele (jmeno, prijmeni, cislo_op, skupina)
                values('" . htmlspecialchars($_POST["first_name"]) . "','" . htmlspecialchars($_POST["last_name"]) . "','" . htmlspecialchars($_POST["id_number"]) . "', '" . htmlspecialchars($_POST["group"]) . "')
            ";

            if (mysqli_query($spojeni, $sql)) {
                header("Location: ./");
            }

            else {
                header("Location: addUser.php?error=1");
            }

            exit();
        }

        else {
            header("Location: addUser.php?error=2");
            exit();
        }
    }

    else if(isset($_GET["error"]) && $_GET["error"]) {
        switch ($_GET["error"]) {
            case 1: 
                $errorMessage = "Držitele se nepodařilo přidat";
                break;

            case 2: 
                $errorMessage = "Vyplňte všechny hodnoty";
                break;
        }
    }


    $sql = "
        SELECT *
        FROM zp_skupiny
    ";

    $skupiny = mysqli_query($spojeni, $sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Přidat držitele zbrojního průkazu</title>
    <link href="./public/style/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <div id="add-form">
        <form method="POST">
            <h1>Přidat držitele</h1>
            <label>
                Zadejte jméno:
                <input type="text" name="first_name" required />
            </label>
            
            <label>
                Zadejte příjmení:
                <input type="text" name="last_name" required />
            </label>
            
            <label>
                Zadejte číslo OP:
                <input type="number" name="id_number" min="10000" required />
            </label>
    
            <label>
                Vyberte skupinu:
                <select name="group">
                    <?php 
                        while($radek = mysqli_fetch_assoc($skupiny)) {
                            echo('<option value="' . $radek["id_skupiny"] . '">' . $radek["nazev"] . '</option>');
                        }
                        ?>
                </select>
            </label>
            <?php 
                if(isset($errorMessage)) {
                    echo('<p class="error-message">' . $errorMessage . '</p>');
                }
            ?>
            <a href="./" class="back"> Zpět</a>
            <button type="submit">Přidat</button>
        </form>
    </div>
</body>
</html>
<?php 
    require("connect.php");

    if(isset($_GET["logout"])) {
        session_unset();
        session_destroy();
        header("Location: ./");
        exit();
    }

    if($_POST) {
        if(
            isset($_POST["name"]) && $_POST["name"] &&
            isset($_POST["password"]) && $_POST["password"]
        ) {
            $sql = "
                SELECT *
                FROM zp_uzivatele
                WHERE jmeno = '" . $_POST["name"] . "'";
    
            $uzivatel = mysqli_query($spojeni, $sql);
            if(mysqli_num_rows($uzivatel)) {
                $uzivatel = mysqli_fetch_assoc($uzivatel);
    
                if(password_verify($_POST['password'], $uzivatel['heslo'])) {
                    $_SESSION["uzivatel"] = $uzivatel['id_uzivatele'];

                    if($uzivatel['admin']) {
                        $_SESSION["uzivatel_admin"] = true;
                    }

                    else {
                        $_SESSION["uzivatel_admin"] = false;
                    }

                    header("Location: index.php");
                    exit();
                }
    
                else {
                    $errorMessage = "Neplaté heslo";
                }
            }
    
            else {
                $errorMessage = "Uživatel s tímto jménem neexistuje";
            }
            
        }
    
        else {
            $errorMessage = "Vyplňte všechny údaje";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Přihlášení</title>
    <link href="./public/style/login.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form method="POST" action="login.php" autocomplete="off">
        <h1>Přihlášení</h1>
        <label>
            Jméno:
            <input type="text" name="name" required/>
        </label>

        <label>
            Heslo:
            <input type="password" name="password" required/>
        </label>
        <?php 
            if(isset($errorMessage)) {
                echo('<p class="error-message">' . $errorMessage . '</p>');
            }
        ?>

        <button type="submit">Přihlásit</button>
    </form>
</body>
</html>
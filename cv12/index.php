<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cvičení 12</title>
    <?php
        if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']) && $_SERVER['PHP_AUTH_USER'] == 'test' && $_SERVER['PHP_AUTH_PW'] == 'test') {
            echo ('Přihlášení proběhlo úspěšně');
        }
        else {
            header('HTTP/1.0 401 Unauthorized');
            header('WWW-Authenticate: Basic realm="Login"');
            echo ('Chyba prihlaseni - zadejte platne uzivatelske jmeno a heslo!');
            exit();
        }
    ?>

</head>
<body>
    <p><a href="../">Rozcesník</a></p>
    <?php    
        $heslo="Hrdlicka";    
        $zakodovane = hash('sha3-512',($heslo));    
        echo ("<p>heslo je: ". $heslo . "</p>");
        echo ("<p>heslo je zakódované šifrou: SHA3-512" . $zakodovane . "</p>");
    ?>
</body>
</html>
# Tvorba internetových stránek

### Rozdělení cvičení

- 1-6 - HTML + CSS
- 7 - JS
- 8 - 12 - PHP

### Popis

Všechna vypracovaná cvičení do předmětu *Tvorba internetových stránek*, všechna cvičení byla hodnocena v rozmezí **90% - 100%**. Pokud narazíte na problém, napište ho [issues](https://gitlab.com/jktech-vspj/tis/-/issues). 

### Stažení

```git
git@gitlab.com:jktech-vspj/tis.git
```

### Použití

#### Požadavky
- Sass Compiler [Pro VS-Code](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass)

#### SCSS
Aby jste mohli zkompilovat SCSS na CSS je potřeba compiler, pokud používáte [Pro VS-Code](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass), otevřete scss soubor úkolu a v pravo dole klikněte na *Watch Sass*

#### Cvičení 1 - 7
Tato cvičení fungují bez apache serveru, pro zpříjemnění vývoje můžete použít [Live server pro VS-Code](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)

#### Cvičení 8 - 12 + projekt
Pro spuštění PHP je potřeba Apache server, PHP, MySQL. Pro windows je nejjednodušší použít [XAMPP](https://www.apachefriends.org/). Zde do složky htdocs (Defaultně *C:/xampp/htdocs/*) je potřeba soubory pro cvičení přesunout. A poté v *XAMPP control panelu* spustit Apache + MySQL. 

Pro naimportování struktury databáze otevřeme phpMyAdmin defaultně na adrese *localhost/phpmyadmin*, uživatelské jméno *root* a heslo je prázdné. Na školním serveru je přihlašovací jméno váš login do IS a heslo dostanete od cvičícího na první hodině. Po přihlášení (Na horním panelu) vybereme **Import**, vybereme soubor **DB.sql** z adresáře projektu **structures/** a dole potvrdíme import. U cvičení kde se pracuje s databází **(CV10 - CV12) je nutné vyplnit údaje k databázi v souboru connect.php**. U projektu je tyto hodoty potřeba vyplnit v souboru *includes/db-connect.php*.

#### Poděkování
- [itnetwork.cz](https://www.itnetwork.cz/)  - Za databázový wrapper použitý v projektu

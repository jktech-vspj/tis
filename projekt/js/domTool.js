'use strict';

class domTools {
    constructor() {}

    init() {
        this.events = new events();
    }

    buildTable(data) {
        if(data.length > 0) {
            let i = 1;
            for(const rowData of data) {
                const rowElement = document.createElement("tr");
    
                const IDColumn = document.createElement("td");
                IDColumn.innerText = rowData.id_drzitele || i;
                rowElement.appendChild(IDColumn);
    
                const nameColumn = document.createElement("td");
                nameColumn.innerText = rowData.jmeno;
                rowElement.appendChild(nameColumn);
    
                const lastNameColumn = document.createElement("td");
                lastNameColumn.innerText = rowData.prijmeni;
                rowElement.appendChild(lastNameColumn);
    
                const IDNumberColumn = document.createElement("td");
                IDNumberColumn.innerText = rowData.cislo_op;
                rowElement.appendChild(IDNumberColumn);
    
                const groupColumn = document.createElement("td");
                groupColumn.innerText = rowData.skupina;
                rowElement.appendChild(groupColumn);

                if(typeof rowData.id_drzitele !== "undefined") {
                    const detailColumn = document.createElement("td");
                    const detailButton = document.createElement("button");
                    detailButton.innerText = "Detail";
                    detailButton.addEventListener("click", () => {
                        const eventsTool = new events();
                        eventsTool.displayDetail(rowData.id_drzitele);
                    });
                    detailColumn.appendChild(detailButton);
                    rowElement.appendChild(detailColumn);
                }

                
                document.querySelector("#holders-table").insertAdjacentElement("beforeend", rowElement);
                i++;
            }
        }

        else {
            const rowElement = document.createElement("tr");
            rowElement.classList.add("empty");

            const columns = document.querySelectorAll("#holders-table th").length;

            const columnElement = document.createElement("td");
            columnElement.colSpan = columns;
            columnElement.innerText = "Držitel nebyl nalezen";
            rowElement.appendChild(columnElement);

            document.querySelector("#holders-table").insertAdjacentElement("beforeend", rowElement);
        }
    }

    clearTable() {
        document.querySelector("#holders-table").querySelectorAll("tr:not(:first-child)").forEach(element => {
            element.remove();
        });
    }

    buildHolderDetail(data, guns) {
        const adminButtons = document.querySelector("#add-holder") ? true : false;
        const detailBox = document.createElement("div");

        detailBox.id = "detail-box";
        detailBox.classList.add("toggle-box");
        detailBox.addEventListener("mousedown", (e) => {
            if(e.target === e.currentTarget) {
                detailBox.remove();
            }
        });

        const detailBoxContent = document.createElement("div");
        detailBoxContent.id = "detail-box-content";
        detailBox.appendChild(detailBoxContent);

        const closeDetailBox = document.createElement("button");
        closeDetailBox.id = "detail-box-close";
        closeDetailBox.classList.add("close-button");
        detailBoxContent.appendChild(closeDetailBox);

        closeDetailBox.addEventListener("click", () => {
            detailBox.remove();
        });

        const detailBoxHeader = document.createElement("div");
        detailBoxHeader.id = "detail-box-header";
        detailBoxContent.appendChild(detailBoxHeader);
        
        detailBoxHeader.appendChild(buildDetailInfo("Křestní jméno", data.jmeno));
        detailBoxHeader.appendChild(buildDetailInfo("Příjmení", data.prijmeni));

        const detailBoxData = document.createElement("div");
        detailBoxData.id = "detail-box-data";
        detailBoxContent.appendChild(detailBoxData);

        detailBoxData.appendChild(buildDetailInfo("ID Držitele", data.id_drzitele));
        detailBoxData.appendChild(buildDetailInfo("Skupina", data.skupina));

        const remainDate = new Date(data.datum_platnosti);
        const statusText = remainDate > new Date ? "Aktivní" : "Neaktivní";
        detailBoxData.appendChild(buildDetailInfo("Platnost", statusText));
        detailBoxData.appendChild(buildDetailInfo("Datum narození", data.datum_narozeni));
        detailBoxData.appendChild(buildDetailInfo("Číslo OP", data.cislo_op));

        detailBoxContent.appendChild(this.buildDetailGunsTable(guns));

        if(adminButtons) {
            const adminToolsBox = document.createElement("div");
            adminToolsBox.id = "detail-box-admin";
            detailBoxContent.appendChild(adminToolsBox);

            const editButton = document.createElement("button");
            editButton.innerText = "Upravit držitele";
            editButton.id = "edit-holder"
            adminToolsBox.appendChild(editButton);
            editButton.addEventListener("click", async () => {
                detailBox.remove();
                this.buildAddHolder();
                this.updateEditHolder(data);
            });

            const addGunButton = document.createElement("button");
            addGunButton.innerText = "Přidat zbraň";
            addGunButton.id = "add-gun"
            adminToolsBox.appendChild(addGunButton);
            addGunButton.addEventListener("click", async () => {
                detailBox.remove();
                this.buildAddGun(data.id_drzitele);
            });

            const removeButton = document.createElement("button");
            removeButton.innerText = "Smazat držitele";
            removeButton.id = "remove-holder"
            adminToolsBox.appendChild(removeButton);
            removeButton.addEventListener("click", async () => {
                const apiTool = new apiTools();
                try {
                    await apiTool.deleteFromApi("holders", [{name: "id", value: data.id_drzitele}]);
                }

                catch(e) {
                    console.error(e);
                }

                window.location.reload();
            });
        }

        document.querySelector("#holders-table").insertAdjacentElement("afterend", detailBox);

        function buildDetailInfo(name, value) {
            const nameBox = document.createElement("span");
            nameBox.classList.add("value-box")
    
            const nameBoxName = document.createElement("h2");
            nameBoxName.innerText = value;
            nameBox.appendChild(nameBoxName);
    
            const nameBoxTitle = document.createElement("h5");
            nameBoxTitle.innerText = name;
            nameBox.appendChild(nameBoxTitle);
    
            return nameBox;
        }
    }

    buildDetailGunsTable(guns) {
        const adminButtons = document.querySelector("#add-holder") ? true : false;
        const table = document.createElement("table");

        const tableHeaderRow = document.createElement("tr");
        table.appendChild(tableHeaderRow);

        let tableColumns;

        if(adminButtons) {
            tableColumns = ["ID zbraně", "Značka", "Typ", "Ráže", "Výrobní číslo", "Odebrat"];
        }
        
        else {
            tableColumns = ["ID zbraně", "Značka", "Typ", "Ráže", "Výrobní číslo"];
        }

        for(const column of tableColumns) {
            const tableHeaderColumn = document.createElement("th");
            tableHeaderColumn.innerText = column;
            tableHeaderRow.appendChild(tableHeaderColumn);
        }

        if(guns.length > 0) {
            for (const gun of guns) {
                const gunsRow = document.createElement("tr");

                const idColumn = document.createElement("td");
                idColumn.innerText = gun.id_zbrane;
                gunsRow.appendChild(idColumn);

                const producerColumn = document.createElement("td");
                producerColumn.innerText = gun.znacka;
                gunsRow.appendChild(producerColumn);

                const modelColumn = document.createElement("td");
                modelColumn.innerText = gun.model;
                gunsRow.appendChild(modelColumn);

                const caliberColumn = document.createElement("td");
                caliberColumn.innerText = gun.raze;
                gunsRow.appendChild(caliberColumn);

                const snColumn = document.createElement("td");
                snColumn.innerText = gun.vyrobni_cislo;
                gunsRow.appendChild(snColumn);

                if(adminButtons) {
                    const removeColumn = document.createElement("td");
                    const removeButton = document.createElement("button");
                    removeButton.innerText = "Smazat";
                    removeButton.addEventListener("click", async () => {
                        const apiTool = new apiTools();
                        try {
                            await apiTool.deleteFromApi("guns", [{name: "id", value: gun.id_zbrane}]);

                            gunsRow.remove();
                            const rows = table.querySelectorAll("tr");
                            if(rows.length <= 1) {
                                const numberOfColumns = table.querySelector("tr").childNodes.length;
                                const emptyRow = document.createElement("tr");
                                const emptyMessage = document.createElement("td");
                                emptyMessage.colSpan = numberOfColumns;
                                emptyMessage.classList.add("empty-message");
                                emptyMessage.innerText = "Držitel nevlastní žádnou zbraň";
                                emptyRow.appendChild(emptyMessage);
                                table.appendChild(emptyRow);
                            }
                        }

                        catch(e) {
                            console.error(e);
                        }
                    });
                    removeColumn.appendChild(removeButton);
                    gunsRow.appendChild(removeColumn);
                }
                
                table.appendChild(gunsRow);
            }
        }

        else {
            const numberOfColumns = table.querySelector("tr").childNodes.length;
            const emptyRow = document.createElement("tr");
            const emptyMessage = document.createElement("td");
            emptyMessage.colSpan = numberOfColumns;
            emptyMessage.classList.add("empty-message");
            emptyMessage.innerText = "Držitel nevlastní žádnou zbraň";
            emptyRow.appendChild(emptyMessage);
            table.appendChild(emptyRow);
        }

        return table;
    }

    buildAddHolder() {
        const addHolderBox = document.createElement("div");
        addHolderBox.classList.add("toggle-box");
        addHolderBox.id = "add-holder-box";

        addHolderBox.addEventListener("mousedown", (e) => {
            if(e.target === e.currentTarget) {
                addHolderBox.remove();
            }
        });

        const addHolderForm = document.createElement("form");
        addHolderForm.method = "POST";
        addHolderForm.action = "./api/holders/";
        addHolderBox.appendChild(addHolderForm);
        addHolderForm.addEventListener("submit", async (e) => {
            if(e.currentTarget.classList.contains("edit-holder-form")) {
                return;
            }
            e.preventDefault();

            const callBack = () => {
                addHolderBox.remove();
                document.querySelector("#reload-table").click();
            }

            const apiTool = new apiTools();
            try {
                apiTool.sendForm(e, callBack);
            }

            catch(e) {
                console.error(e);
            }
        });

        const addHolderCloseButton = document.createElement("button");
        addHolderCloseButton.classList.add("close-button");
        addHolderCloseButton.type = "button";
        addHolderForm.appendChild(addHolderCloseButton);
        addHolderCloseButton.addEventListener("click", () => {
            addHolderBox.remove();
        });

        addHolderForm.appendChild(this.buildInput("text", "name", "Zadejte jméno"));
        addHolderForm.appendChild(this.buildInput("text", "last_name", "Zadejte příjmení"));
        addHolderForm.appendChild(this.buildInput("number", "ic_number", "Zadejte číslo OP"));

        const groupLabel = document.createElement("label");
        const selectGroup = document.createElement("select");
        selectGroup.name = "group_id"
        const groupLabelSpan = document.createElement("span");
        groupLabelSpan.innerText = "Vyberte skupinu";

        groupLabel.appendChild(selectGroup);
        groupLabel.appendChild(groupLabelSpan);
        addHolderForm.appendChild(groupLabel);

        async function buildSelectGroup() {
            const apiTool = new apiTools();
            const groups = await apiTool.getFromApi("groups");
            for(const group of groups) {
                const option = document.createElement("option");
                option.innerText = group.nazev;
                option.value = group.id_skupiny;
                selectGroup.appendChild(option);
            }
        }

        buildSelectGroup();

        addHolderForm.appendChild(this.buildInput("date", "birth_date", "Zadejte datum narození"));
        addHolderForm.appendChild(this.buildInput("date", "valid_date", "Zadejte datum platnosti"));

        const submitButton = document.createElement("button");
        submitButton.type = "submit";
        submitButton.innerText = "Přidat";
        addHolderForm.appendChild(submitButton);


        document.querySelector("#holders-table").insertAdjacentElement("afterend", addHolderBox);
        
        addHolderForm.querySelector("input").focus();
    }

    buildAddGun(userID) {
        const addGunBox = document.createElement("div");
        addGunBox.classList.add("toggle-box");
        addGunBox.id = "add-gun-box";

        addGunBox.addEventListener("mousedown", (e) => {
            if(e.target === e.currentTarget) {
                addGunBox.remove();
            }
        });

        const addGunForm = document.createElement("form");
        addGunForm.method = "POST";
        addGunForm.action = "./api/guns/";
        addGunBox.appendChild(addGunForm);
        addGunForm.addEventListener("submit", async (e) => {
            e.preventDefault();

            const callBack = () => {
                addGunBox.remove();

                new events().displayDetail(userID);
            }

            const apiTool = new apiTools();
            try {
                apiTool.sendForm(e, callBack);
            }

            catch(e) {
                console.error(e);
            }
        });

        const addHolderCloseButton = document.createElement("button");
        addHolderCloseButton.classList.add("close-button");
        addHolderCloseButton.type = "button";
        addGunForm.appendChild(addHolderCloseButton);
        addHolderCloseButton.addEventListener("click", () => {
            addGunBox.remove();
        });

        addGunForm.appendChild(this.buildInput("text", "manufacturer", "Zadejte výrobce"));
        addGunForm.appendChild(this.buildInput("text", "model", "Zadejte model"));
        addGunForm.appendChild(this.buildInput("text", "caliber", "Zadejte ráži"));
        addGunForm.appendChild(this.buildInput("text", "serial_number", "Sériové číslo"));
        
        const holderIdBox = document.createElement("input");
        holderIdBox.type = "hidden";
        holderIdBox.name = "holder";
        holderIdBox.value = userID;
        addGunForm.appendChild(holderIdBox);
        

        const submitButton = document.createElement("button");
        submitButton.type = "submit";
        submitButton.innerText = "Přidat";
        addGunForm.appendChild(submitButton);

        document.querySelector("#holders-table").insertAdjacentElement("afterend", addGunBox);
        addGunForm.querySelector("input").focus();
    }

    buildInput(type, name, placeHolder) {
        const label = document.createElement("label");

        const input = document.createElement("input");
        input.placeholder = " ";
        input.name = name;
        input.type = type;
        input.required = true;
        label.appendChild(input);

        const span = document.createElement("span");
        span.innerText = placeHolder;
        label.appendChild(span);

        if(type === "number") {
            input.min = "100000000";
            input.max = "999999999";
        }

        return label;
    }

    updateEditHolder(holderData) {
        const editHolderBox = document.querySelector("#add-holder-box");
        const editHolderForm = editHolderBox.querySelector("form");
        editHolderForm.classList.add("edit-holder-form")
        editHolderForm.method = "PUT";

        editHolderForm.addEventListener("submit", async (e) => {
            e.preventDefault();
            e.stopPropagation();

            const apiTool = new apiTools();

            const callBack = () => {
                editHolderBox.remove();
                
                new events().displayDetail(holderData.id_drzitele);
            }

            try {
                apiTool.sendForm(e, callBack);
            }

            catch(e) {
                console.error(e);
            }
        });

        editHolderForm.querySelector("input[name=\"name\"]").value = holderData.jmeno;
        editHolderForm.querySelector("input[name=\"last_name\"]").value = holderData.prijmeni;
        editHolderForm.querySelector("input[name=\"ic_number\"]").value = holderData.cislo_op;

        editHolderForm.querySelector("input[name=\"birth_date\"]").value = holderData.datum_narozeni;
        editHolderForm.querySelector("input[name=\"valid_date\"]").value = holderData.datum_platnosti;

        const idElement = document.createElement("input");
        idElement.name = "id";
        idElement.type = "hidden";
        idElement.value = holderData.id_drzitele;
        editHolderForm.appendChild(idElement);

        editHolderForm.querySelector("button[type=\"submit\"]").innerText = "Upravit";
    }
}
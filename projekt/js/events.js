'use strict';

class events {
    constructor () {}

    init() {
        this.apiTool = new apiTools();
        this.domTool = new domTools();

        this.loginForm = document.querySelector("#login-box-form");
        this.loginForm !== null ? this.setLoginForm() : null;

        this.logoutButton = document.querySelector("#logout-button");
        this.logoutButton !== null ? this.setLogout() : null;

        this.searchInput = document.querySelector("#search-name-input");
        this.searchInput !== null ? this.setSearch() : null;

        this.addHolderButton = document.querySelector("#add-holder");
        this.addHolderButton !== null ? this.setAddHolder() : null;

        this.setTableEvents();
    }

    setLoginForm() {
        const loginFormBox = document.querySelector("#toggle-login-box");
        this.loginForm.addEventListener("submit", async (e) => {
            e.preventDefault();
            this.loginForm.querySelectorAll("p.error-message").forEach(element => {
                element.remove();
            });

            try {
                await this.apiTool.login(e);
            }

            catch(e) {
                const errorMessage = document.createElement("p");
                errorMessage.classList.add("error-message");
                errorMessage.innerText = e.reason;
                this.loginForm.insertAdjacentElement("beforeend", errorMessage)
            }
        });

        loginFormBox.addEventListener("mousedown", (e) => {
            if(e.target === e.currentTarget) {
                loginFormBox.classList.remove("active");
            }
        });

        loginFormBox.querySelector("#close-toggle-login").addEventListener("click", (e) => {
            loginFormBox.classList.remove("active");
        });

        document.querySelector("#login-button").addEventListener("click", (e) => {
            loginFormBox.classList.add("active");
            loginFormBox.querySelector("input[name=\"name\"]")?.focus();
        });
    }

    setLogout() {
        this.logoutButton.addEventListener("click", async () => {
            this.apiTool.logout();
        });
    }

    setSearch() {
        this.searchInput.addEventListener("keyup", async (e) => {
            const query = e.currentTarget.value;
            if(query.length > 0) {
                const response = await this.apiTool.getFromApi("holders", [{name: "search", value: query}]);
                this.domTool.clearTable();
                this.domTool.buildTable(response);
            }

            else {
                const response = await this.apiTool.getFromApi("holders");
                this.domTool.clearTable();
                this.domTool.buildTable(response);
            }
        });

        document.querySelector("#clear-search").addEventListener("click", async () => {
            this.searchInput.value = null;

            const response = await this.apiTool.getFromApi("holders");
            this.domTool.clearTable();
            this.domTool.buildTable(response);
        });
    }

    async displayDetail(id) {
        const apiTool = new apiTools();
        const domTool = new domTools();
        const responseHolder = await apiTool.getFromApi("holders", [{name: "id", value: id}]);
        const responseGuns = await apiTool.getFromApi("guns", [{name: "holder", value: id}]);
        domTool.buildHolderDetail(responseHolder, responseGuns);
    }

    setTableEvents() {
        const table = document.querySelector("#holders-table");
        const tableHeader = table.querySelector("tr:first-child");

        tableHeader.querySelectorAll("th[data-order-name]").forEach(element => {
            element.addEventListener("click", async (e) => {
                //Remove prev order class
                const targetElement = e.currentTarget;

                const orderColumn = targetElement.dataset.orderName;
                const apiTool = new apiTools();
                let data;
                const domTool = new domTools();

                if(targetElement.classList.contains("order-asc")) {
                    element.classList.remove("order-asc");
                    element.classList.add("order-desc");

                    data = await apiTool.getFromApi("holders", [{name: "order", value: "-" + orderColumn}]);
                }

                else {
                    tableHeader.querySelectorAll("th.order-asc, th.order-desc").forEach(element => {
                        element.classList.remove("order-asc");
                        element.classList.remove("order-desc");
                    });
                    
                    targetElement.classList.add("order-asc");
                    data = await apiTool.getFromApi("holders", [{name: "order", value: orderColumn}]);
                }


                domTool.clearTable();
                domTool.buildTable(data); 
            });
        });

        const reloadButton = document.querySelector("#reload-table");
        reloadButton.addEventListener("click", async () => {
            tableHeader.querySelectorAll("th.order-asc, th.order-desc").forEach(element => {
                element.classList.remove("order-asc");
                element.classList.remove("order-desc");
            });

            const apiTool = new apiTools();
            const data = await apiTool.getFromApi("holders");
            
            const domTool = new domTools();
            domTool.clearTable();
            domTool.buildTable(data); 
        });
    }

    setAddHolder() {
        this.addHolderButton.addEventListener("click", () => {
            const domTool = new domTools();
            domTool.buildAddHolder();
        });
    }
}
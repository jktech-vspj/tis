'use strict';

class apiTools {
    constructor() {}

    async testApi() {
        const response = await fetch(`./api/holders/`);
        if(response.status !== 200) {
            console.error("Error while testing API connection");
        }
    }

    async login(e) {
        return new Promise((resolve, reject) => {
            const loginForm = e.currentTarget;
            const FD = new URLSearchParams(new FormData(loginForm));
            const submitButton = e.submitter;
            submitButton.disabled = true;

            const XHR = new XMLHttpRequest();
            submitButton.disabled = false;

            XHR.onreadystatechange = () => {
                if (XHR.readyState === XMLHttpRequest.DONE) {
                    
                    const status = XHR.status;
                    if (status === 0 || (status >= 200 && status < 400)) {
                        window.location.reload();
                        resolve();
                    }
                    
                    else {
                        console.log(XHR.responseText)
                        reject(JSON.parse(XHR.responseText));
                    }
                }
            }

            XHR.open(`POST`, `./api/login/`);
            XHR.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            XHR.send(FD);
        });
    }

    async logout() { 
        return new Promise((resolve, reject) => {
            const XHR = new XMLHttpRequest();

            XHR.onreadystatechange = () => {
                if (XHR.readyState === XMLHttpRequest.DONE) {
                    
                    const status = XHR.status;
                    if (status === 0 || (status >= 200 && status < 400)) {
                        window.location.reload();
                        resolve();
                    }
                    
                    else {
                        reject();
                    }
                }
            }

            XHR.open(`POST`, `./api/login/`);
            XHR.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            XHR.send("logout");
        });
    }

    sendForm(e, callBack) {
        return new Promise((resolve, reject) => {
            const formElement = e.currentTarget;
            const FD = new URLSearchParams(new FormData(formElement));
            const submitButton = e.submitter;
            submitButton.disabled = true;
            const XHR = new XMLHttpRequest();

            XHR.onreadystatechange = () => {
                if (XHR.readyState === XMLHttpRequest.DONE) {
                    const status = XHR.status;
                    if (status === 0 || (status >= 200 && status < 400)) {
                        callBack();
                        resolve();
                    }

                    else {
                        submitButton.disabled = false;
                        reject();
                    }
                }
            }

            XHR.open(formElement.getAttribute("method"), formElement.action);
            XHR.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            XHR.send(FD);
        });
    }

    postToApi(apiName, params) {
        return new Promise((resolve, reject) => {
            const XHR = new XMLHttpRequest();
            const keys = params.map((item) => {
                return item['name'] + "=" + item['value'];
            });

            XHR.onreadystatechange = () => {
                if (XHR.readyState === XMLHttpRequest.DONE) {
                    const status = XHR.status;

                    if (status === 0 || (status >= 200 && status < 400)) {
                        resolve();
                    }

                    else {
                        reject();
                    }
                }
            }

            XHR.open("POST", `./api/${apiName}`);
            XHR.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            XHR.send(keys.join("&"));
        });
    }

    getFromApi(apiName, params = []) {
        return new Promise((resolve, reject) => {
            const XHR = new XMLHttpRequest();

            const keys = params.map((item) => {
                return item['name'] + "=" + item['value'];
            });

            XHR.onreadystatechange = () => {
                if (XHR.readyState === XMLHttpRequest.DONE) {
                    const status = XHR.status;
                    if (status === 0 || (status >= 200 && status < 400)) {
                        const response = XHR.responseText;

                        try {
                            const values = JSON.parse(response);
                            resolve(values);
                        }

                        catch(e) {
                            console.error("Invalid api response format");
                            reject();
                        }
                    } 
                    
                    else {
                        reject();
                    }
                }
            }

            XHR.open("GET", `./api/${apiName}?${keys.join("&")}`);
            XHR.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            XHR.send();
        });
    }

    deleteFromApi(apiName, params) {
        return new Promise((resolve, reject) => {
            const XHR = new XMLHttpRequest();
            const keys = params.map((item) => {
                return item['name'] + "=" + item['value'];
            });

            XHR.onreadystatechange = () => {
                if (XHR.readyState === XMLHttpRequest.DONE) {
                    const status = XHR.status;

                    if (status === 0 || (status >= 200 && status < 400)) {
                        resolve();
                    }

                    else {
                        reject();
                    }
                }
            }

            XHR.open("DELETE", `./api/${apiName}`);
            XHR.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            XHR.send(keys.join("&"));
        });
    }
}
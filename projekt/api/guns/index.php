<?php
    require_once("../../includes/db-connect.php");

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET,POST,DELETE");
    header("Access-Control-Max-Age: 3600");

    if(!(isset($_SESSION["uzivatel"]) && $_SESSION["uzivatel"])) {
        echo('{"message": "Unauthorized"}');
        http_response_code(401);
        exit();
    }

    if($_SERVER['REQUEST_METHOD'] === "GET") {
        //Get guns for specific holder
        if(isset($_GET["holder"]) && $_GET["holder"] && is_numeric($_GET["holder"])) {
            $guns = Db::queryAll("
                SELECT *
                FROM projekt_zbrane
                WHERE drzitel = ?
            ", $_GET["holder"]);

            if($guns) {
                echo(json_encode($guns));
            }

            else {
                echo("[]");
            }
        }

        else if(isset($_GET["id"]) && $_GET["id"] && is_numeric($_GET["id"])) {
            $gun = Db::queryAll("
                SELECT *
                FROM projekt_zbrane
                WHERE id_zbrane = ?
            ", $_GET["id"]);

            if($gun) {
                echo(json_encode($gun));
            }

            else {
                echo("[]");
            }
        }

        else {
            $gun = Db::queryAll("
                SELECT *
                FROM projekt_zbrane
            ", $_GET["id"]);

            echo(json_encode($gun));
        }
    }

    else if($_SERVER['REQUEST_METHOD'] === "POST") {
        if(isset($_SESSION['uzivatel']) && $_SESSION['uzivatel']) {
            if(isset($_SESSION['uzivatel_admin']) && $_SESSION['uzivatel_admin']) {
                if(
                    isset($_POST["manufacturer"]) && $_POST["manufacturer"] && 
                    isset($_POST["model"]) && $_POST["model"] && 
                    isset($_POST["caliber"]) && $_POST["caliber"] && 
                    isset($_POST["serial_number"]) && $_POST["serial_number"] && 
                    isset($_POST["holder"]) && $_POST["holder"] && is_numeric($_POST["holder"])
                ) {
                    $addStatus = Db::query("
                        INSERT INTO projekt_zbrane (znacka, model, raze, vyrobni_cislo, drzitel)
                        VALUES (?,?,?,?,?)
                    ", $_POST["manufacturer"], $_POST["model"], $_POST["caliber"], $_POST["serial_number"], $_POST["holder"]);

                    if($addStatus === 1) {
                        echo('{"message": "OK"}');
                        http_response_code(200);
                    }

                    else {
                        echo('{"message": "Internal Server Error"}');
                        http_response_code(500);
                    }
                }
            }

            else {
                echo('{"message": "Forbidden"}');
                http_response_code(403);
            }   
        }

        else {
            echo('{"message": "Unauthorized"}');
            http_response_code(401);
        }
    }

    else if($_SERVER['REQUEST_METHOD'] === "DELETE") {
        $data = file_get_contents("php://input");
        parse_str($data, $_DELETE);

        if(isset($_SESSION['uzivatel']) && $_SESSION['uzivatel']) {
            if(isset($_SESSION['uzivatel_admin']) && $_SESSION['uzivatel_admin']) {
                if(isset($_DELETE["id"]) && $_DELETE["id"] && is_numeric($_DELETE["id"])) {
                    $removeStatus = Db::query("
                        DELETE FROM projekt_zbrane
                        WHERE id_zbrane = ?
                    ", $_DELETE["id"]);

                    if($removeStatus === 1) {
                        echo('{"message": "OK"}');
                        http_response_code(200);
                    }

                    else {
                        echo('{"message": "Bad Request"}');
                        http_response_code(400);
                    }
                }
            }

            else {
                echo('{"message": "Forbidden"}');
                http_response_code(403);
            }   
        }

        else {
            echo('{"message": "Unauthorized"}');
            http_response_code(401);
        }
    }

    else {
        echo('{"message": "Method Not Allowed"}');
        http_response_code(405);
    }
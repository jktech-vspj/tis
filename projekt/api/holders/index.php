<?php
    require_once("../../includes/db-connect.php");

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE");
    header("Access-Control-Max-Age: 3600");

    //TODO login verify
    if($_SERVER['REQUEST_METHOD'] === "GET") {
        if(isset($_GET["id"]) && $_GET["id"] && is_numeric($_GET["id"])) {
            if(isset($_SESSION["uzivatel"]) && $_SESSION["uzivatel"]) {
                $user = Db::queryOne("
                    SELECT *, projekt_skupiny.nazev as skupina
                    FROM projekt_drzitele
                    INNER JOIN projekt_skupiny ON projekt_skupiny.id_skupiny = projekt_drzitele.skupina
                    WHERE id_drzitele = ?
                ", $_GET["id"]);

                if($user) {
                    echo(json_encode($user));
                }

                else {
                    echo('{"message": "Bad Request"}');
                    http_response_code(400);
                }
            }

            else {
                echo('{"message": "Unauthorized"}');
                http_response_code(401);
            }
        }

        else if(isset($_GET["search"]) && $_GET["search"]) {
            if(isset($_SESSION["uzivatel"]) && $_SESSION["uzivatel"]) {
                if(str_contains($_GET["search"], " ")) {
                    $searchSplit = explode(" ",$_GET["search"]);
                    $user = Db::queryAll("
                        SELECT *, projekt_skupiny.nazev as skupina
                        FROM projekt_drzitele
                        INNER JOIN projekt_skupiny ON projekt_skupiny.id_skupiny = projekt_drzitele.skupina
                        WHERE jmeno LIKE ? AND prijmeni LIKE ?
                    ", "%" . $searchSplit[0] . "%", "%" . $searchSplit[1] . "%");
                }
                
                else {
                    $user = Db::queryAll("
                        SELECT *, projekt_skupiny.nazev as skupina
                        FROM projekt_drzitele
                        INNER JOIN projekt_skupiny ON projekt_skupiny.id_skupiny = projekt_drzitele.skupina
                        WHERE jmeno LIKE ? OR prijmeni LIKE ? OR cislo_op LIKE ?
                    ", "%" . $_GET["search"] . "%", "%" . $_GET["search"] . "%", "%" . $_GET["search"] . "%");
                }


                echo(json_encode($user));
            }

            else {
                echo('{"message": "Unauthorized"}');
                http_response_code(401);
            }
        }

        else {
            //Set limit variable
            $limit = 10000;

            if(isset($_GET["limit"]) && $_GET["limit"] && is_numeric($_GET["limit"])) {
                $limit = $_GET["limit"];
            }

            $allowedOrder = [
                "id" => "id_drzitele",
                "age" => "datum_narozeni",
                "name" => "jmeno",
                "last_name" => "prijmeni",
                "group" => "projekt_skupiny.nazev"
            ];

            //Set order variable
            $order = "id_drzitele";
            $sort = "ASC";

            
            if(isset($_GET["order"]) && $_GET["order"]) {
                if (substr($_GET["order"], 0, 1) === "-") {
                    $orderName = substr($_GET["order"], 1);
                    $sort = "DESC";
                }

                else {
                    $orderName = $_GET["order"];
                }

                if($allowedOrder[$orderName]) {
                    $order = $allowedOrder[$orderName];
                }
            }

            if(isset($_SESSION['uzivatel']) && $_SESSION['uzivatel']) {
                if(isset($_SESSION['uzivatel_admin']) && $_SESSION['uzivatel_admin']) {
                    $response = Db::queryAll("
                        SELECT projekt_drzitele.*, projekt_skupiny.nazev AS skupina
                        FROM projekt_drzitele
                        INNER JOIN projekt_skupiny ON projekt_skupiny.id_skupiny = projekt_drzitele.skupina
                        ORDER BY " . $order . " " . $sort . "
                        LIMIT ?
                    ", $limit);
                }

                else {
                    $response = Db::queryAll("
                        SELECT projekt_drzitele.*, projekt_skupiny.nazev AS skupina
                        FROM projekt_drzitele
                        INNER JOIN projekt_skupiny ON projekt_skupiny.id_skupiny = projekt_drzitele.skupina
                        ORDER BY " . $order . " " . $sort . "
                        LIMIT ?
                    ", $limit);
                }
            }

            else {
                $response = Db::queryAll("
                    SELECT projekt_drzitele.jmeno, projekt_drzitele.prijmeni, projekt_drzitele.datum_narozeni, projekt_drzitele.datum_platnosti, projekt_skupiny.nazev AS skupina
                    FROM projekt_drzitele
                    INNER JOIN projekt_skupiny ON projekt_skupiny.id_skupiny = projekt_drzitele.skupina
                    ORDER BY " . $order . " " . $sort . "
                    LIMIT ?
                ", $limit);

                foreach ($response as &$record) {
                    $record["jmeno"] = $record["jmeno"][0] . "******";
                    $record["prijmeni"] = $record["prijmeni"][0] . "******";
                    $record["cislo_op"] = "******";
                }
            }

            echo(json_encode($response));
        }
    }

    else if($_SERVER['REQUEST_METHOD'] === "POST") {
        if(isset($_SESSION['uzivatel']) && $_SESSION['uzivatel']) {
            if(isset($_SESSION['uzivatel_admin']) && $_SESSION['uzivatel_admin']) {
                if(
                    isset($_POST["name"]) && $_POST["name"] &&
                    isset($_POST["last_name"]) && $_POST["last_name"] &&
                    isset($_POST["ic_number"]) && $_POST["ic_number"] && is_numeric($_POST["ic_number"]) &&
                    isset($_POST["group_id"]) && $_POST["group_id"] && is_numeric($_POST["group_id"]) &&
                    isset($_POST["birth_date"]) && $_POST["birth_date"] &&
                    isset($_POST["valid_date"]) && $_POST["valid_date"]
                ) {
                    $insertStatus = Db::query("
                        INSERT INTO projekt_drzitele (jmeno, prijmeni, cislo_op, skupina, datum_narozeni, datum_platnosti)
                        VALUES (?,?,?,?,?,?)
                    ", $_POST["name"], $_POST["last_name"], $_POST["ic_number"], $_POST["group_id"], $_POST["birth_date"], $_POST["valid_date"]);
                    
                    if($insertStatus === 1) {
                        echo('{"message": "OK"}');
                        http_response_code(200);
                    }

                    else {
                        echo('{"message": "Internal Server Error"}');
                        http_response_code(500);
                    }
                }

                else {
                    echo('{"message": "Bad Request"}');
                    http_response_code(400);
                }
            }

            else {
                echo('{"message": "Forbidden"}');
                http_response_code(403);
            }
        }

        else {
            echo('{"message": "Unauthorized"}');
            http_response_code(401);
        }
    }

    else if($_SERVER['REQUEST_METHOD'] === "PUT") {
        if(isset($_SESSION['uzivatel']) && $_SESSION['uzivatel']) {
            parse_str(file_get_contents('php://input'), $_PUT);
            if(isset($_SESSION['uzivatel_admin']) && $_SESSION['uzivatel_admin']) {
                if(
                    isset($_PUT["name"]) && $_PUT["name"] &&
                    isset($_PUT["last_name"]) && $_PUT["last_name"] &&
                    isset($_PUT["ic_number"]) && $_PUT["ic_number"] && is_numeric($_PUT["ic_number"]) &&
                    isset($_PUT["group_id"]) && $_PUT["group_id"] && is_numeric($_PUT["group_id"]) &&
                    isset($_PUT["birth_date"]) && $_PUT["birth_date"] &&
                    isset($_PUT["valid_date"]) && $_PUT["valid_date"] &&
                    isset($_PUT["id"]) && $_PUT["id"] && is_numeric($_PUT["id"])

                ) {
                    $updateStatus = Db::query("
                        UPDATE projekt_drzitele 
                        SET jmeno = ?, prijmeni = ?, cislo_op = ?, skupina = ?, datum_narozeni = ?, datum_platnosti = ?
                        WHERE id_drzitele = ?
                    ", $_PUT["name"], $_PUT["last_name"], $_PUT["ic_number"], $_PUT["group_id"], $_PUT["birth_date"], $_PUT["valid_date"], $_PUT["id"]);
                    
                    echo('{"message": "OK"}');
                    http_response_code(200);
                }

                else {
                    echo('{"message": "Bad Request"}');
                    http_response_code(400);
                }
            }

            else {
                echo('{"message": "Forbidden"}');
                http_response_code(403);
            }
        }

        else {
            echo('{"message": "Unauthorized"}');
            http_response_code(401);
        }
    }

    else if($_SERVER['REQUEST_METHOD'] === "DELETE") {
        if(isset($_SESSION['uzivatel']) && $_SESSION['uzivatel']) {
            if(isset($_SESSION['uzivatel_admin']) && $_SESSION['uzivatel_admin']) {
                $data = file_get_contents("php://input");
                parse_str($data, $_DELETE);

                if(isset($_DELETE["id"]) && $_DELETE["id"] && is_numeric($_DELETE["id"])) {
                    $deleteStatus = Db::query("
                        DELETE FROM projekt_drzitele
                        WHERE id_drzitele = ?
                    ", $_DELETE["id"]);

                    Db::query("
                        DELETE FROM projekt_zbrane
                        WHERE drzitel = ?
                    ", $_DELETE["id"]);

                    if($deleteStatus === 1) {
                        echo('{"message": "User deleted"}');
                        http_response_code(200);
                    }

                    else {
                        echo('{"message": "Invalid user ID"}');
                        http_response_code(400);
                    }
                }

                else {
                    echo('{"message": "Bad Request"}');
                    http_response_code(400);
                }

            }

            else {
                echo('{"message": "Forbidden"}');
                http_response_code(403);
            }
        }

        else {
            echo('{"message": "Unauthorized"}');
            http_response_code(401);
        }
    }

    else {
        echo('{"message": "Method Not Allowed"}');
        http_response_code(405);
    }
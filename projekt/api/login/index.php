<?php
    require_once("../../includes/db-connect.php");

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");

    if($_SERVER['REQUEST_METHOD'] === "POST") {
        if(isset($_POST["name"]) && $_POST["name"] && isset($_POST["password"]) && $_POST["password"]) {
            //Login

            $uzivatel = Db::queryOne("
                SELECT *
                FROM projekt_uzivatele
                WHERE nazev = ?
            ", $_POST["name"]);

            if($uzivatel) {
                $passwordVerify = password_verify($_POST["password"], $uzivatel["heslo"]);

                if($passwordVerify) {
                    if($uzivatel["admin"] === NULL) {
                        $_SESSION["uzivatel_admin"] = false;
                    }

                    else {
                        $_SESSION["uzivatel_admin"] = true;
                    }

                    $_SESSION["uzivatel"] = $uzivatel["nazev"];

                    http_response_code(200);
                    echo('{"message": "OK"}');
                }

                else {
                    http_response_code(400);
                    echo('{"message": "Bad Request", "reason": "Neplatné heslo"}');
                }
            }

            else {
                http_response_code(400);
                echo('{"message": "Bad Request", "reason": "Uživatel nebyl nalazen"}');
            }
        }

        else if(isset($_POST["logout"])) {
            //From PHP manual
            $_SESSION = array();

            if (ini_get("session.use_cookies")) {
                $params = session_get_cookie_params();
                setcookie(session_name(), '', time() - 42000,
                    $params["path"], $params["domain"],
                    $params["secure"], $params["httponly"]
                );
            }

            session_destroy();
        }

        else {
            http_response_code(400);
            echo('{"message": "Bad Request"}');
        }
    }

    else {
        http_response_code(405);
        echo('{"message": "Method Not Allowed"}');
    }
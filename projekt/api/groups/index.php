<?php
    require_once("../../includes/db-connect.php");

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Max-Age: 3600");

    if($_SERVER['REQUEST_METHOD'] === "GET") {
        if(isset($_SESSION['uzivatel']) && $_SESSION['uzivatel']) {
            $groups = Db::queryAll("
                SELECT *
                FROM projekt_skupiny
            ");
        }

        else {
            $groups = Db::queryAll("
                SELECT nazev
                FROM projekt_skupiny
            ");
        }

        echo(json_encode($groups));
    }

    else {
        echo('{"message": "Method Not Allowed"}');
        http_response_code(405);
    }
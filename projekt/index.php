<?php 
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Databáze držitelů zbrojního průkazu - Projekt TIS</title>
    <link href="./style/style.css" type="text/css" rel="stylesheet">
</head>
<body>
<h1>Databáze držitelů zbrojního průkazu</h1>

    <div id="data-controls">
        <span>
            <button id="reload-table" title="Obnovit data"></button>
            <?php
                if(isset($_SESSION["uzivatel_admin"]) && $_SESSION["uzivatel_admin"]) {
                    echo('<button class="add-holder" id="add-holder">Přidat držitele</button>');
                }
            ?>
        </span>
        <label>
        <?php 
            if(isset($_SESSION["uzivatel"]) && $_SESSION["uzivatel"]) {
                echo('
                    <input type="text" id="search-name-input" placeholder="Vyhledat podle jména, příjmení nebo čísla op..." />
                    <button id="clear-search"></button>
                ');
            }

            else {
                echo('
                    <span>Pro zobrazení dat se přihlaste</span>
                ');
            }
        ?>
        </label>
        <span>
            <?php 
                if(isset($_SESSION["uzivatel"]) && $_SESSION["uzivatel"]) {
                    echo('
                        <span id="user-icon" title="' . $_SESSION["uzivatel"] . '">' . $_SESSION["uzivatel"][0] . '</span>
                        <button class="logout" id="logout-button" title="Odhlásit se"></button>
                    ');
                }
                
                else {
                    echo('<button class="login" id="login-button" title="Přihlásit se"></button>');
                }
            ?>
        </span>
    </div>

    <table id="holders-table">
        <tr>
            <?php 
                if(isset($_SESSION["uzivatel"]) && $_SESSION["uzivatel"]) {
                    echo('<th data-order-name="id">ID</th>');
                } 

                else {
                    echo('<th>#</th>');
                }
            ?>
            <th data-order-name="name">Jméno</th>
            <th data-order-name="last_name">Přijmení</th>
            <th>Číslo OP</th>
            <th data-order-name="group">Skupina</th>
            <?php 
                if(isset($_SESSION["uzivatel"]) && $_SESSION["uzivatel"]) {
                    echo('<th>Detail</th>');
                }
            ?>
        </tr>
    </table>

<?php 
    if(!(isset($_SESSION["uzivatel"]) && $_SESSION["uzivatel"])) {
?>

<div id="toggle-login-box" class="toggle-box">
        <form action="./api/login/" method="POST" id="login-box-form">
            <button id="close-toggle-login" class="close-button" type="button"></button>
            <label>
                <input type="text" name="name" placeholder=" " required />
                <span>Zadejte jméno</span>
            </label>

            <label>
                <input type="password" name="password" placeholder=" " required />
                <span>Zadejte heslo</span>
            </label>

            <button type="submit">Přihlásit</button>
        </form>
    </div>
<?php 
    }
?>

    <script src="./js/apiTool.js"></script>
    <script src="./js/domTool.js"></script>
    <script src="./js/events.js"></script>
    <script>
        document.addEventListener("DOMContentLoaded", async () => {
            const apiTool = new apiTools();
            const domTool = new domTools();
            const eventTool = new events();
            domTool.init();
            eventTool.init();
            const response = await apiTool.getFromApi("holders");
            domTool.buildTable(response);
        })
    </script>
</body>
</html>
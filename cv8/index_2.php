<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Osmé cvičení - 2</title>
    <style>
        #output {
            margin: 50px auto 0 auto;
            justify-content: center;
            border: 1px solid black;
            align-items: center;
            border-radius: 100%;
            display: flex;
            height: 120px;
            width: 120px;
        }

    </style>
</head>
<body>
    <p><a href="../">Rozcesník</a></p>
    <h1>Výpočet obvodu kruhu</h1>
    <form method="post">
        <input type="number" name="polomer" placeholder="Zadejte poloměr..." required>

        <button type="submit">Vypočítat</button>
    </form>
    <?php
        if($_POST && $_POST["polomer"]) {
            $obvod = vypocetObvodu($_POST["polomer"]);
            echo('
                <div id="output">o = ' . $obvod . 'j</div>
            ');
        }

        function vypocetObvodu($polomer) {
            return round(2 * pi() * $polomer, 3);
        }
    ?>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Osmé cvičení - 3</title>
</head>
<body>
    <p><a href="../">Rozcesník</a></p>
    <h1>Výpočet ceny jízdného</h1>
    <form method="post">
        <input type="number" name="vzdalenost" min="1" max="20" placeholder="Zadejte vydálenost..." required>
        <button type="submit">Vypočítat</button>
    </form>
    <?php
        if($_POST && $_POST["vzdalenost"] != null) {
            if(!is_numeric($_POST["vzdalenost"]) || $_POST["vzdalenost"] <= 0 || $_POST["vzdalenost"] > 20 ) {
                echo("<p>Neplatná vstupní hodnota</p>");
                return;
            }

            else if($_POST["vzdalenost"] <= 2) {
                $cena = 4;
            }

            else if ($_POST["vzdalenost"] <= 5) {
                $cena = 6;
            }

            else if ($_POST["vzdalenost"] <= 10) {
                $cena = 10;
            }

            else if ($_POST["vzdalenost"] <= 20) {
                $cena = 15;
            }

            echo("<p>Cena jízdenky je " . $cena . " Kč</p>");
        }
    ?>
</body>
</html>
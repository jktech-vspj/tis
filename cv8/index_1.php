<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Osmé cvičení - 1</title>
</head>
<body>
    <p><a href="../">Rozcesník</a></p>
    <form method="post">
        <select name="translate_phrase">
            <option>Orange</option>
            <option>Green</option>
            <option>Red</option>
            <option>Blue</option>
            <option>Purple</option>
        </select>

        <button type="submit">Přeložit</button>
    </form>
    <?php 
        $translateArray = Array(
            "Orange" => "Oranžová",
            "Green" => "Zelená",
            "Red" => "Červená",
            "Blue" => "Modrá",
            "Purple" => "Fialová"
        );

        if($_POST && $_POST["translate_phrase"]) {
            $translated = $translateArray[$_POST["translate_phrase"]];
            if($translated) {
                echo("<p>" . $_POST["translate_phrase"] . " => " . $translateArray[$_POST["translate_phrase"]] . "</p>");
            }

            else {
                echo("<p>Zadané slovo nelze přeložit</p>");
            }

        }
    ?>
</body>
</html>
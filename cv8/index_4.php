<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Osmé cvičení - 4</title>
</head>
<body>
    <p><a href="../">Rozcesník</a></p>
    <h1>Získání IP adresy klienta</h1>
    <form>
        <input type="hidden" name="ip" value="<?php echo($_SERVER["REMOTE_ADDR"]); ?>">
        <button type="submit">Získat IP</button>
    </form>
    <?php
        if(isset($_GET["ip"]) && !empty($_GET["ip"])) {
            echo("<p>Vaše IP je: " . $_GET["ip"] ."</p>");
        }
    ?>
</body>
</html>